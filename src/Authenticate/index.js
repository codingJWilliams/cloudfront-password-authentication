const params = require('./params.json');
const fs = require("fs");
const cookie = require("./cookie");
const jwt = require("jsonwebtoken");

exports.handler = async (event) => {
    const { request } = event.Records[0].cf;

    if (request.uri === `/_password_auth/authorize` && request.method.toUpperCase() === "GET") {
        return {
            status: 403,
            body: fs.readFileSync("./password.html", 'utf-8').replace(/%LOGO%/g, params.logo).replace(/%LOGO_FONT%/g, params.logo_font)
        }
    } else if (request.uri === `/_password_auth/authorize` && request.method.toUpperCase() === "POST") {
        const data = request.body.encoding === 'base64' ? Buffer.from(request.body.data, 'base64').toString() : request.body.data;
        const postData = new URLSearchParams(data);
        const decodedQuery = new URLSearchParams(request.querystring);

        console.log(postData);

        if (postData.get('password') === params.password) {
            const cookieAttributes = {
                domain: undefined,
                expires: new Date(Date.now() + 30 * 864e+5),
                secure: true,
                path: '/',
                httpOnly: true,
                sameSite: true,
            };
            const verifyToken = jwt.sign({
                exp: (Date.now() / 1000) + (30 * 24 * 60 * 60),
                nonce: Date.now()
            }, params.verify_key);
            const cookies = [
                cookie.Cookies.serialize(`password.verifyToken`, verifyToken, cookieAttributes)
            ];

            
            return {
                status: '302',
                headers: {
                    'location': [{
                        key: 'Location',
                        value: decodedQuery.get('redirect'),
                    }],
                    'cache-control': [{
                        key: 'Cache-Control',
                        value: 'no-cache, no-store, max-age=0, must-revalidate',
                    }],
                    'pragma': [{
                        key: 'Pragma',
                        value: 'no-cache',
                    }],
                    'set-cookie': cookies.map(c => ({ key: 'Set-Cookie', value: c })),
                }
            };
        } else {
            return {
                status: 403,
                body: fs.readFileSync("./password-incorrect.html", 'utf-8').replace(/%LOGO%/g, params.logo).replace(/%LOGO_FONT%/g, params.logo_font)
            }
        }
    }

    return request;
}