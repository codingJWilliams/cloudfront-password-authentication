resource "null_resource" "authenticate-dependencies" {
  provisioner "local-exec" {
    command = "npm install"
    working_dir = "${path.module}/src/Authenticate"
  }

  triggers = {
    dependencies_versions = filemd5("${path.module}/src/Authenticate/package.json")
  }
}

module "authenticate" {
  source = "git::https://gitlab.com/littlewonders/terraform/lambda-at-edge.git?ref=v1.0.0"

  functions = {
    authenticate = {
      source       = "${path.module}/src/Authenticate"
      runtime      = "nodejs14.x"
      handler      = "index.handler"
      event_type   = "viewer-request"
      include_body = true
      params = {
        verify_key = random_password.jwtkey.result
        password   = var.password
        logo = var.login_page_logo
        logo_font = var.login_page_font
      }
    }
  }

  providers = {
    aws = aws.us-east-1
  }

  context = module.this.context

  depends_on = [
    null_resource.authenticate-dependencies
  ]
}