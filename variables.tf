variable "context" {
  type        = any
  description = "Context of a cloudposse/label resource"
  default     = {}
}

variable "name" {
  type        = string
  description = "Name to generate labels for resources"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "profile" {
  type        = string
  description = "AWS profile"
}

variable "password" {
  type        = string
  description = "Password to allow access to the Distribution"
  sensitive = true
}

variable "login_page_font" {
  default = "Akshar"
  type = string
  description = "Google font name"
}

variable "login_page_logo" {
  type = string
  description = "Page title"
  default = "Protected Page"
}

variable "cloudfront_endpoint" {
  type        = string
  description = "Domain that is configured with Cloudfront, eg. https://mysite.com"
}

variable "any_origin" {
  type        = string
  description = "Name of an origin on your Cloudfront distribution. Required for the cache behaviour to be generated"
}

variable "verify_template_location" {
  type        = string
  default     = ""
  description = "Which file to use for the Verify function"
}