output "lambda_function_association" {
  value = module.authenticate.lambda_function_association
}

output "function_id" {
  value = aws_cloudfront_function.verify.arn
}


data "aws_cloudfront_cache_policy" "CachingDisabled" {
  name = "Managed-CachingDisabled"
}

data "aws_cloudfront_origin_request_policy" "AllViewer" {
  name = "Managed-AllViewer"
}

output "cache_behaviour" {
  value = {
    path_pattern                = "/_password_auth/*"
    allowed_methods             = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods              = ["GET", "HEAD", "OPTIONS"]
    target_origin_id            = var.any_origin
    cache_policy_id             = data.aws_cloudfront_cache_policy.CachingDisabled.id
    origin_request_policy_id    = data.aws_cloudfront_origin_request_policy.AllViewer.id
    compress                    = true
    viewer_protocol_policy      = "redirect-to-https"
    lambda_function_association = module.authenticate.lambda_function_association
    // Un-needed but required variables
    forward_cookies                   = null
    default_ttl                       = 0
    function_association              = []
    forward_cookies_whitelisted_names = []
    forward_header_values             = []
    forward_query_string              = null
    max_ttl                           = 0
    min_ttl                           = 0
    response_headers_policy_id        = null
    trusted_key_groups                = null
    trusted_signers                   = null
  }
}
